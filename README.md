Privacy Policy

Last updated: June 26, 2016

This page informs you of our policies regarding the collection, use and disclosure of Personal Information we receive from users of the BonBouton application.

We use your Personal Information only for providing and improving BonBouton. By using BonBouton, you agree to the collection and use of information in accordance with this policy.

How We Use Your Information We use information you provide to authenticate you and deliver content to you and from you. While we collect this information, we only use it to provide you with an incredible experience in our application! We do not share this information.

Information Collection And Use To use our application, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to your name ("Personal Information"). We ask you to sign in to our application with an email account.


Security The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.

Changes To This Privacy Policy This Privacy Policy is effective as of June 26, 2016 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page.

We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Your continued use of the Service after we post any modifications to the Privacy Policy on this page will constitute your acknowledgment of the modifications and your consent to abide and be bound by the modified Privacy Policy.

If we make any material changes to this Privacy Policy, we will notify you either through the email Account you have provided us, or by placing a notice on our website.

Contact Us If you have any questions about this Privacy Policy, please contact us on Twitter: hello@bonbouton.co